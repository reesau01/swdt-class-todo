git clone git@bitbucket.org:reesau01/swdt-class-todo.git
dir=$(find swdt-class-todo -type f)
echo $dir
for i in $dir
do
	echo $i
	if [[ $i == *.pyc ]]; then
		rm $i
		echo $i has been deleted
	elif [[ $i == *.class ]]; then
		rm $i
		echo $i has been deleted
	elif [[ $i == *.out ]]; then
		rm $i
		echo $i has been deleted
	elif [[ $i == *.txt ]]; then
		rm $i
		echo $i has been deleted
	fi
done
dirPath="$(locate -br swdt-class-todo)"
cd $dirPath
git add .
git commit -m "Repository is tidy"
codePath="$(locate bitbucket.sh)"
cp $codePath bitbucket.sh
git add .
git commit -m "bitbucket.sh has been added"
git push
